/*
 * @ReportView:全局类型
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-02 13:05:13
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-13 13:42:53
 * @copyright: Copyright (c) 2021, FengChuangke
 */
export interface BackgroundStyle {
  background?: string
  backgroundImage?: string
  backgroundColor?: string
}

export interface SizeStyle {
  width?: string | number
  height?: string | number
  minWidth?: string | number
  minHeight?: string | number
}

export interface ColorStyle {
  color?: string
}

export interface CascadeDataItem {
  label: string
  value: string
}

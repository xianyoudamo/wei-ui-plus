/*
 * @ReportView:图片类型
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-04 11:24:16
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-04 11:24:17
 * @copyright: Copyright (c) 2021, FengChuangke
 */
declare module '*.svg'
declare module '*.png'
declare module '*.jpg'
declare module '*.jpeg'
declare module '*.gif'
declare module '*.bmp'
declare module '*.tiff'

/*
 * @ReportView:组件类型
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-03 12:39:40
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-17 15:28:27
 * @copyright: Copyright (c) 2021, FengChuangke
 */

import type { Plugin } from 'vue'
export interface VuePlugin {
  name?: string
  displayName?: string
}

declare module '@xwsx/wei-ui-plus' {
  const Zoom: Plugin
  const RedirectLinkButton: Plugin
  const WeiUi: Plugin
  export { Zoom, RedirectLinkButton }
  export default WeiUi
}

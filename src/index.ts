/*
 * @ReportView:组件库入口文件
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-14 17:24:25
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-17 15:26:34
 * @copyright: Copyright (c) 2021, FengChuangke
 */
import type { App, Plugin } from 'vue'
import Zoom from '@ui/components/Zoom'
import RedirectLinkButton from '@ui/components/RedirectLinkButton'

// 存储组件列表
const components: Plugin[] = [Zoom, RedirectLinkButton]

// 接收 Vue 作为参数。如果使用 use 注册插件，则所有的组件都将被注册
const WeiUi: Plugin = {
  install(app: App): void {
    // 遍历注册全局组件
    components.forEach((component) => app.use(component))
  }
}

export { Zoom, RedirectLinkButton }

export default WeiUi

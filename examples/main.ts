import { createApp } from 'vue'
import App from './App.vue'
import WeiUi from '../src'
const app = createApp(App)
app.use(WeiUi)
app.mount('#app')

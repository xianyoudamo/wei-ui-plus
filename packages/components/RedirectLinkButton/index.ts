/*
 * @ReportView:跳转链接按钮组件
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-02 12:01:11
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-17 12:54:49
 * @copyright: Copyright (c) 2021, FengChuangke
 */
import RedirectLinkButton from './src/main.vue'
import { withInstall } from '@ui/utils/helper'

const RedirectLinkButtonPlugin = withInstall(RedirectLinkButton)

export default RedirectLinkButtonPlugin

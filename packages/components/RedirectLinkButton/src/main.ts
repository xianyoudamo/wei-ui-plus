/*
 * @ReportView: 跳转按钮
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-02 12:04:43
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-16 22:54:11
 * @copyright: Copyright (c) 2021, FengChuangke
 */
import { defineComponent, computed } from 'vue'
import { useRouter } from 'vue-router'
import { BackgroundStyle } from 'types/global'

const props = {
  // 按钮的图片名称F
  icon: { type: String, default: '' },
  // 是否返回上一页
  isBack: { type: Boolean, default: false },
  // 要跳转到的链接
  path: { type: String, default: null },
  controlled: { type: Boolean, default: false },
  // 是否禁用跳转能力
  disabled: { type: Boolean, default: false }
}

export default defineComponent({
  name: 'RedirectLinkButton',
  props,
  emits: ['click'],
  setup(props, { emit }) {
    const router = useRouter()
    /**
     * Computed
     */

    const backgroundStyle = computed((): BackgroundStyle => {
      return { background: `url('${props.icon}') center center no-repeat` }
    })

    /**
     * Methods
     */

    const handleClick = (): void => {
      const { disabled, isBack, controlled, path } = props
      if (disabled) return
      if (isBack) {
        return router.go(-1)
      }
      if (controlled) {
        emit('click')
      } else {
        path && router.push(path)
      }
    }

    return {
      backgroundStyle,
      handleClick
    }
  }
})

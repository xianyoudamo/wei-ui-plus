/*
 * @ReportView:放大缩小按钮组件
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-02 11:35:16
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-17 12:14:52
 * @copyright: Copyright (c) 2021, FengChuangke
 */
import { defineComponent, computed, toRef, unref } from 'vue'
import RedirectLinkButton from '../../RedirectLinkButton/src/main.vue'
import bigIconByte from '@ui/assets/icon/big-icon.svg'
import smallIconByte from '@ui/assets/icon/small-icon.svg'

/**
 * Props
 */
const props = {
  mouseControl: { type: Boolean, default: false }
}

export default defineComponent({
  name: 'Zoom',
  components: { RedirectLinkButton },
  props,
  emits: [
    'enlarge',
    'narrow',
    'start-enlarge',
    'end-enlarge',
    'start-narrow',
    'end-narrow'
  ],
  setup(props, { emit }) {
    const bigIcon = computed((): unknown => {
      return bigIconByte
    })

    const smallIcon = computed((): unknown => {
      return smallIconByte
    })

    const mouseControlRef = toRef(props, 'mouseControl')
    const mouseControl = unref(mouseControlRef)

    const handleEnlarge = (): void => {
      if (!mouseControl) return
      emit('enlarge')
    }

    const handleNarrow = (): void => {
      if (!mouseControl) return
      emit('narrow')
    }

    const handleStartEnlarge = (): void => {
      if (!mouseControl) return
      emit('start-enlarge')
    }

    const handleEndEnlarge = (): void => {
      if (!mouseControl) return
      emit('end-enlarge')
    }

    const handleStartNarrow = (): void => {
      if (!mouseControl) return
      emit('start-narrow')
    }

    const handleEndNarrow = (): void => {
      if (!mouseControl) return
      emit('end-narrow')
    }

    return {
      bigIcon,
      smallIcon,
      handleEnlarge,
      handleNarrow,
      handleStartEnlarge,
      handleEndEnlarge,
      handleStartNarrow,
      handleEndNarrow
    }
  }
})

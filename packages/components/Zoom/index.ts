/*
 * @ReportView:放大缩小按钮组件
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-14 19:13:39
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-17 12:53:47
 * @copyright: Copyright (c) 2021, FengChuangke
 */
import Zoom from './src/main.vue'
import { withInstall } from '@ui/utils/helper'

const ZoomPlugin = withInstall(Zoom)

export default ZoomPlugin

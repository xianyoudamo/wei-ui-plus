/*
 * @ReportView:工具函数
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-13 11:21:24
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-16 17:22:01
 * @copyright: Copyright (c) 2021, FengChuangke
 */
import { VuePlugin } from 'types/index'
import type { App, Plugin } from 'vue'
/**
 * 获取全局唯一标识符
 * @param {Number} len uuid的长度
 * @param {Boolean} firstU 将返回的首字母置为"u"
 * @param {Nubmer} radix 生成uuid的基数(意味着返回的字符串都是这个基数),2-二进制,8-八进制,10-十进制,16-十六进制
 */
export function guid(len = 32, firstU = true, radix = null): string {
  const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('')
  const uuid = []
  const currentRadix = radix || chars.length

  if (len) {
    // 如果指定uuid长度,只是取随机的字符,0|x为位运算,能去掉x的小数位,返回整数位
    for (let i = 0; i < len; i++) uuid[i] = chars[0 | (Math.random() * currentRadix)]
  } else {
    let r
    // rfc4122标准要求返回的uuid中,某些位为固定的字符
    // eslint-disable-next-line no-multi-assign
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-'
    uuid[14] = '4'

    for (let i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | (Math.random() * 16)
        uuid[i] = chars[i === 19 ? (r & 0x3) | 0x8 : r]
      }
    }
  }
  // 移除第一个字符,并用u替代,因为第一个字符为数值时,该guuid不能用作id或者class
  if (firstU) {
    uuid.shift()
    return `u${uuid.join('')}`
  } else {
    return uuid.join('')
  }
}

export const withInstall = <T>(component: T, alias?: string) => {
  const comp = component as unknown as VuePlugin & Plugin
  comp.install = (app: App) => {
    app.component((comp.name || comp.displayName) as string, component)
    if (alias) {
      app.config.globalProperties[alias] = component
    }
  }
  return component as T & Plugin
}

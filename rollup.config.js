/*
 * @ReportView:rollup配置文件
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-03 10:58:53
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-17 14:59:14
 * @copyright: Copyright (c) 2021, FengChuangke
 */
// rollup.config.js
import { terser } from 'rollup-plugin-terser'
import { nodeResolve } from '@rollup/plugin-node-resolve'
import vue from 'rollup-plugin-vue'
import scss from 'rollup-plugin-scss'
import babel from 'rollup-plugin-babel'
import typescript from 'rollup-plugin-typescript2'
import commonjs from '@rollup/plugin-commonjs'
import replace from 'rollup-plugin-replace'
import alias from '@rollup/plugin-alias'
import cleanup from 'rollup-plugin-cleanup'
import path from 'path'
// import image from '@rollup/plugin-image'
import url from '@rollup/plugin-url'

function resolve(dir) {
  return path.join(__dirname, dir)
}
export default {
  input: 'src/index.ts',
  external: ['vue', 'vue-router'],
  output: [
    {
      file: 'dist/wei-ui-plus.esm.js',
      format: 'esm',
      name: 'wei-ui-plus.esm'
    },
    {
      file: 'dist/wei-ui-plus.umd.js',
      format: 'umd',
      name: 'wei-ui-plus.umd',
      globals: {
        vue: 'Vue'
      }
    }
  ],
  plugins: [
    nodeResolve({
      extensions: ['.js', '.ts']
    }),

    replace({
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    alias({
      entries: [
        { find: '@ui', replacement: resolve('packages') },
        { find: '@', replacement: resolve('examples') },
        { find: '@src', replacement: resolve('src') }
      ]
    }),

    typescript(),
    url(),
    vue(),
    babel({
      extensions: ['.vue', '.ts', '.js', '.tsx', '.jsx'],
      exclude: 'node_modules/**',
      presets: ['@vue/babel-preset-jsx'],
      runtimeHelpers: true
    }),
    scss({ output: 'dist/bundle.css' }),
    commonjs(),
    terser(),
    cleanup()
  ]
}

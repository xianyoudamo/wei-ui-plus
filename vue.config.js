/*
 * @ReportView:webpack配置文件
 * @Author: FengChuangke <235065@qq.com>
 * @Date: 2022-03-01 16:21:22
 * @LastEditors: FengChuangke <235065@qq.com>
 * @LastEditTime: 2022-03-14 17:21:03
 * @copyright: Copyright (c) 2021, FengChuangke
 */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  pages: {
    index: {
      entry: 'examples/main.ts',
      template: 'public/index.html',
      filename: 'index.html'
    }
  },
  publicPath: '.',
  runtimeCompiler: true,
  configureWebpack: {
    resolve: {
      extensions: ['.vue', '.ts'],
      alias: {
        '@ui': resolve('packages'),
        '@': resolve('examples'),
        '@src': resolve('src'),
        types: resolve('typings')
      }
    }
  }
}
